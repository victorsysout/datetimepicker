# Sysout Datetimepicker

Modificações no plugin de datetimepicker Tempus Dominus + pickers nativos para mobile.

## Pré-requisitos
*  JQuery *(jquery)*
*  Bootstrap *(bootstrap)*
*  Tempus Dominus *(tempusdominus-bootstrap-4)*
*  Moment *(moment)*
*  JQuery Mask Plugin *(jquery-mask-plugin)*
*  Gulp Sass *(gulp-sass)*

## Como usar
`<datepicker></datepicker>`
`<datetimepicker></datetimepicker/>`

## Parâmetros & Configuração
*  `data-id`: define o id do grupo (quando não especificado, é gerado um aleatório);
*  `data-name`: define o nome que será repassado ao input;
*  `data-format`: define o formato que a data deve aparecer no campo ([padrão moment](https://momentjscom.readthedocs.io/en/latest/moment/04-displaying/01-format/));
*  `data-value`: define o valor que será repassado ao input (valor inicial);
*  `db-format`: especifica o formato do valor auxiliar que será enviado junto com o valor selecionado ([padrão moment](https://momentjscom.readthedocs.io/en/latest/moment/04-displaying/01-format/));
*  `data-min`: especifica a data mínima que pode ser selecionada;
*  `data-max`: especifica a data máxima que pode ser selecionada;
*  `data-linked-min`: especifica qual elemento (id) deve receber como data mínima o valor deste input;
*  `data-linked-max`: especifica qual elemento (id) deve receber como data máxima o valor deste input;
*  `required`: determina se o input deve ser obrigatório;

### Observações
*  **data-id**: quando não especificado, é gerado um aleatório;
*  **data-name**: quando não especificado, é utilizado o mesmo valor do id;
*  **data-format**: o padrão para a tag `datepicker` é DD/MM/YYYY e o padrão para a tag `datetimepicker` é DD/MM/YYYY HH:mm;
*  **data-value, data-min, data-max**: devem ser especificados no formato esperado *(data-format)*;
*  **db-format**: funcionalidade criada para facilitar, visto que o tempus-dominus envia o valor da forma que está no input e para salvar no banco temos que converter para o "padrão internacional". O valor default desse atributo é YYYY-MM-DD HH:mm:ss. O nome do campo que será enviado é o nome do campo informado concatenado com a string "_db";
*  * **atenção!** o valor recebido no request não será sempre o mesmo, visto que o tempus-dominus envia de um jeito (que será no formato passado na configuração) e o seletor nativo (apesar de exibir a data formatada) envia o valor no "formato internacional", por isso é interessante utilizar sempre o valor recebido do campo auxiliar;
 
## Exemplo de pickers linkados

```
<div class="row">
    <div class="col-6">
        <datetimepicker data-id="date1" data-name="date1" data-linked-min="date2"></datetimepicker>
    </div>
    <div class="col-6">
        <datepicker data-id="date2" data-name="date2" data-linked-max="date1"></datepicker>
    </div>
</div>
```

## Eventos
Os eventos atualizam manualmente o input (seja o valor do campo ou as datas mínimas e máximas). Para todos esses eventos é necessário passar uma data moment;

*  **update-date**: seta um valor (data) para o campo;
*  **update-min**: seta a data mínima para o campo;
*  **update-max**: seta a data máxima para o campo;

```
<datepicker data-id="sysout" class="date1"></datepicker>

var format = 'DD/MM/YYYY';
var dateMinString = '21/10/1995';
var dateMaxString = '25/10/2019';
var dateCurrent = '13/09/2010';

var dateMin = moment(dateMinString, format);
var dateMax = moment(dateMaxString, format);
var date = moment(dateCurrent, format);

$(".date1").trigger("update-min", dateMin);
$(".date1").trigger("update-max", dateMax);
$(".date1").trigger("update-date", date);
```


