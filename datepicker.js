"use strict";

$(document).ready(function() {

    var DATEPICKER_ICON = 'far fa-calendar-alt';

    function initializeDatePicker(picker, format, mask, nativeType) {

        var $datepicker = $(picker);
        var $wrapper;
        
        var mode; // 1: web (tempus dominus), 2: mobile (native)
        var id = uniqid();
        var name;
        var initialValue = $datepicker.data("value");
        var dbFormat = 'YYYY-MM-DD HH:mm:ss';
        var typeDateFormat = "YYYY-MM-DD";
        var typeDatetimeFormat = "YYYY-MM-DDTHH:mm";

        if ($datepicker.data("format")) {
            format = $datepicker.data("format");
        }

        if ($datepicker.data("mask")) {
            mask = $datepicker.data("mask");
        }
 
        if ($datepicker.data("id")) {
            id = $datepicker.data("id");
        
        } else {
            name = id;
        }

        if ($datepicker.data("name")) {
            name = $datepicker.data("name");
        }

        if ($datepicker.attr("db-format")) {
            dbFormat = $datepicker.attr("db-format");
        }

        // Carregar seletores de data nativos
        if (isMobile.any()) {

            var baseHTML = `
            <div class="datepicker-wrapper datepicker-mobile">
                <div class="mobile-group" id="${ id }">
                    <input type="${ nativeType }" name="${ name }" data-parsley-errors-container="${ '#' + id + '-error' }" class="form-control main-input" />
                    <input type="hidden" name="${ name }_db" class="form-control db-input" />
                </div>
                <div id="${ id + '-error' }"></div>
            </div>`;

            $wrapper = $(baseHTML);
            mode = 2;

        // Carregar Tempus Dominus
        } else {

            var baseHTML = `
            <div class="datepicker-wrapper">
                <div class="input-group" data-target-input="nearest" id="${ id }">
                    <input type="text" name="${ name }" data-parsley-errors-container="${ '#' + id + '-error' }" class="form-control datetimepicker-input main-input" data-target="${ '#' + id }" />
                    <input type="hidden" name="${ name }_db" class="form-control db-input" />
                    <div class="input-group-append" data-target="${ '#' + id }" data-toggle="datetimepicker">
                        <button class="btn btn-primary" type="button"><i class="${ DATEPICKER_ICON }"></i></button>
                    </div>
                </div>
                <div id="${ id + '-error' }"></div>
            </div>`;

            $wrapper = $(baseHTML);
            mode = 1;

            var options = {
                locale: "pt-br",
                debug: false,
                format: format,
                useCurrent: false,
                sideBySide: true
            }
        }

        // Adicionar estrutura no HTML
        $datepicker.html($wrapper);
        $datepicker[0].mode = mode;

        var $input = $datepicker.find("input.main-input");
        var $dbInput = $datepicker.find("input.db-input");

        // Ações realizadas somente na web
        if (mode == 1) {

            if (mask) {
                $input.mask(mask);
            }

            var $group = $datepicker.find(".input-group");
            $datepicker[0].group = $group;

            // Carregar plugin apenas depois de adicionar no HTML
            $group.datetimepicker(options);

            var current;

            $group.on("date:changed", function(e, date) {

                var min = $datepicker.attr("data-linked-min");
                var max = $datepicker.attr("data-linked-max");

                if (min && $('#' + min).length > 0) {
                    $('#' + min).trigger("update-min", date);

                } else if (max && $('#' + max).length > 0) {
                    $('#' + max).trigger("update-max", date);
                }

                if (date && date.isValid()) {
                    $dbInput.val(date.format(dbFormat)); // atualizar input auxiliar

                // Limpar campos
                } else {
                    $group.find("input").val("");
                }

                current = $input.val();

            });

            $group.on("change.datetimepicker", function(e) {

                var value = $input.val();

                if (e.date) {
                    $group.trigger("date:changed", e.date);

                // Provavelmente valor foi atualizado manualmente, e o evento acima não foi triggado
                } else if (value && value != current) {
                    $group.trigger("date:changed", moment(value, format));
                }

            });

            $input.on("focus", function() {
                $datepicker.addClass("focused");
            
            }).on("focusout, blur", function() {
                $datepicker.removeClass("focused");
            });

        // Ações realizadas somente mobile
        } else if (mode == 2) {

            $input.change(function() {

                var value = this.value;
                var date;

                if (value) {

                    var type = $input.attr("type");
    
                    if (type == "date") {
                        date = moment(value, typeDateFormat);
    
                    } else if (type == "datetime-local") {
                        date = moment(value, typeDatetimeFormat);
                    }
                }

                if (date) {

                    var min = $datepicker.attr("data-linked-min");
                    var max = $datepicker.attr("data-linked-max");
    
                    if (min && $('#' + min).length > 0) {
                        $('#' + min).trigger("update-min", date);
    
                    } else if (max && $('#' + max).length > 0) {
                        $('#' + max).trigger("update-max", date);
                    }

                    $dbInput.val(date.format(dbFormat));
                
                } else {
                    $dbInput.val("");
                }

            });

        }

        if ($datepicker.attr("required")) {
            $input.attr("required", "required");
        }

        $datepicker.on("update-min", function(e, date) {

            // web
            if (this.mode == 1) {

                if (date) {
                    this.group.datetimepicker("minDate", date);

                } else {
                    this.group.datetimepicker("minDate", false);
                }

            // mobile
            } else if (this.mode == 2) {

                if (date) {

                    var type = $input.attr("type");
    
                    if (type == "date") {
                        $input.attr("min", date.format(typeDateFormat));
    
                    } else if (type == "datetime-local") {
                        $input.attr("data-min", date.format(typeDatetimeFormat));
                    }

                } else {
                    $input.removeAttr("min");
                }
            }

        });

        $datepicker.on("update-max", function(e, date) {

            // web
            if (this.mode == 1) {
                this.group.datetimepicker("maxDate", date);

            // mobile
            } else if (this.mode == 2) {

                var type = $input.attr("type");

                if (type == "date") {
                    $input.attr("max", date.format(typeDateFormat));

                } else if (type == "datetime-local") {
                    $input.attr("data-max", date.format(typeDatetimeFormat));
                }
            }

        });

        if ($datepicker.attr("data-min")) {
            $datepicker.trigger("update-min", moment($datepicker.attr("data-min"), format));
        }

        if ($datepicker.attr("data-max")) {
            $datepicker.trigger("update-max", moment($datepicker.attr("data-max"), format));
        }

        $datepicker.on("update-date", function(e, date) {

            // web
            if (this.mode == 1) {

                setTimeout(function() {
                    $group.datetimepicker("date", date);
                });

            // mobile
            } else if (this.mode == 2) {

                var type = $input.attr("type");
                var datevalue;

                if (type == "date") {
                    datevalue = date.format(typeDateFormat);

                } else if (type == "datetime-local") {
                    datevalue = date.format(typeDatetimeFormat);
                }

                $input.val(datevalue).change(); // necessário para atualizar dbInput

            }

        });
        
        if (initialValue) {
            $datepicker.trigger("update-date", moment(initialValue, format));
        }
    }

    $("datetimepicker").each(function() {
        initializeDatePicker(this, "DD/MM/YYYY HH:mm", "99/99/9999 99:99", "datetime-local");
    });

    $("datepicker").each(function() {
        initializeDatePicker(this, "DD/MM/YYYY", "99/99/9999", "date");
    });

    // Necessário para burlar problema com a validação de min/max do parsley
    $("[type='datetime-local']").each(function() {
        
        var $input = $(this);

        $input.focus(function() {
            $input.attr("min", $input.attr("data-min"));
            $input.attr("max", $input.attr("data-max"));
        });

        $input.focusout(function() {
            $input.removeAttr("min");
            $input.removeAttr("max");
        });

        $input.change(function() {
            $input.blur();
        });

    });

});

var datepickerIcons = {
    time: 'far fa-clock',
    date: 'far fa-calendar',
    up: 'fas fa-chevron-up',
    down: 'fas fa-chevron-down',
    previous: 'fas fa-chevron-left',
    next: 'fas fa-chevron-right',
    today: 'far fa-calendar-check-o',
    clear: 'far fa-trash',
    close: 'far fa-times'
}

$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    icons: datepickerIcons
});

function uniqid() {
    return Math.random().toString(36).substr(2, 9);
}